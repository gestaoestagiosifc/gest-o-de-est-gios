## **Sistema de gestão de Estágios ||  IF- Catarinense** ##

*Desenvolvido pela Fábrica de Software - IFC Campus Araquari*

* Breve resumo

* Versão do Sistema
           1.0
* Configurações

* Dependências:
    Para executar a impressão, necessário instalar:

        sudo apt-get install texlive-latex-base texlive-latex-extra texlive-latex-recommended texlive-fonts-recommended 

        sudo pip install django-latex

       [Referência Django-latex](https://gitorious.org/si-fai/si-ai/commit/b7fefb54f9e97b20ec75b10fe4cf1427cf1cce69#src/si/views/adherent.py)

* Configuração do Banco de Dados

* Equipe de contato