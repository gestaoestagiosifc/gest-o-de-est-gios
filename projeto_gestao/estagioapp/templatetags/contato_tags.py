from django import template
register = template.Library()


@register.filter
def contato_linha(form, key):
    return form.contato_keys[int(key)]


@register.filter
def contato_field(form, key):
    return form.fields[key].widget.render(form, form.fields[key].initial,form.fields[key].widget.attrs)

@register.filter
def contato_field_error(form, key):
    return form[key].errors