from django import template
from django.conf import settings
from django.db.models import Q
from django.contrib.auth.models import Group


register = template.Library()

@register.filter
def check_group(user):
    group = Group.objects.filter(~Q(id=settings.PERM_GRUPO_COORDENADOR), 
    							  Q(id=settings.PERM_GRUPO_EMPRESA))
    
    return True if group[0] in user.groups.all() else False

@register.filter
def check_group_aluno(user):
	group = Group.objects.filter(~Q(id=settings.PERM_GRUPO_COORDENADOR), 
    							 ~Q(id=settings.PERM_GRUPO_EMPRESA), 
    							  Q(id=settings.PERM_GRUPO_ALUNO))

	return True if group[0] in user.groups.all() else False 

@register.filter
def check_group_aluno_coordenador(user):
	group = Group.objects.filter(Q(id=settings.PERM_GRUPO_COORDENADOR) |
								 Q(id=settings.PERM_GRUPO_ALUNO), 
								~Q(id=settings.PERM_GRUPO_EMPRESA))

	return True if group[0] in user.groups.all() else False 

@register.filter
def check_group_empresa_coordenador(user):
	group = Group.objects.filter(Q(id=settings.PERM_GRUPO_COORDENADOR) |
								 Q(id=settings.PERM_GRUPO_EMPRESA), 
								~Q(id=settings.PERM_GRUPO_ALUNO))

	
	user_groups = user.groups.all()
	if group[0] in user_groups or group[1] in user_groups:
		return True
	else:
		return False

@register.filter
def check_group_empresa(user):
	group = Group.objects.filter(~Q(id=settings.PERM_GRUPO_COORDENADOR),
								 ~Q(id=settings.PERM_GRUPO_ALUNO), 
								 Q(id=settings.PERM_GRUPO_EMPRESA))

	return True if group[0] in user.groups.all() else False	