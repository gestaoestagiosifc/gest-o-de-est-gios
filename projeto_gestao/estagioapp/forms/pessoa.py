#!/usr/bin/env python
# -*- coding: utf-8 -*
from django import forms
from django.contrib.auth.models import User
from django.db.models import Q
from estagioapp.models import Pessoa


class FormPessoa(forms.Form):
    # Campos da Auth_user
    usuario = forms.EmailField(max_length=None, widget=forms.TextInput(attrs={'id':'usuario', 'placeholder':'login do usuário'}))
    nome_responsavel = forms.CharField(max_length=None, widget=forms.TextInput(
        attrs={'id': 'nome_responsavel', 'placeholder':'Nome do responsável (Somente Letras)'}))

    senha = forms.CharField(widget=forms.PasswordInput(attrs={'id': 'senha'}))
    confirma_senha = forms.CharField(widget=forms.PasswordInput(attrs={'id': 'confirma_senha'}))

    # Campo da Pessoa
    cpf = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'000.000.000-00', 'id':'cpf'}))

    def clean_confirma_senha(self):

        if 'senha' in self.cleaned_data:

            senha = self.cleaned_data['senha']
            confirma_senha = self.cleaned_data['confirma_senha']

            if senha == confirma_senha:
                return confirma_senha
            else:
                raise forms.ValidationError('Senha não confere')
        else:
            raise forms.ValidationError('Senha não confere')

    def clean_nome_responsavel(self):
        nome = self.cleaned_data['nome_responsavel']
        nome = nome.replace(" ","")
        if nome.isalpha():
            return nome
        else:
            raise forms.ValidationError('Somente letras')

    def clean_cpf(self):
        cpf = self.cleaned_data['cpf']

        buscaCpf = Pessoa.objects.filter(cpf=cpf)

        if buscaCpf.count() > 0:
            raise forms.ValidationError('Cpf já cadastrado')
        else:
            return cpf
