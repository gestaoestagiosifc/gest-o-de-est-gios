#!/usr/bin/env python
# -*- coding: utf-8 -*
from django import forms
from estagioapp.models import Cidade, Endereco, Estado


class FormEndereco(forms.Form):
    # Campo de Endereco
    rua = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'id': 'rua',  'placeholder': 'Nome da Rua'}))
    numero = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'id': 'numero',  'placeholder': 'número/complemento'}))
    bairro = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'id': 'bairro', 'placeholder': 'Nome do Bairro'}))
    cep = forms.CharField(widget=forms.TextInput(attrs={'id':'cep','placeholder': '00000-000'}))
    referencia = forms.CharField(required=False, widget=forms.TextInput(attrs={'id': 'referencia', 'placeholder': 'Por favor, insira uma referência'}))
    estado = forms.ModelChoiceField(queryset=Estado.objects.all())
    cidade = forms.ModelChoiceField(queryset=Cidade.objects.all())

    def save_endereco(self, id=None):

        dados = self.cleaned_data

        # ==== Query para buscar o id da Cidade, pois o self.cleaned_data só manda o valor.
        cidade_id = self.data['cidade']

        if id:
            endereco = Endereco.objects.get(pk=id)
        else:
            endereco = Endereco()

        endereco.rua = dados["rua"]
        endereco.numero = dados["numero"]
        endereco.bairro = dados["bairro"]
        endereco.cep = dados["cep"]
        endereco.referencia = dados["referencia"]
        endereco.cidade_id = cidade_id

        endereco.save()

        return endereco