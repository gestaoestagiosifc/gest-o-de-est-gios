#!/usr/bin/env python
# -*- coding: utf-8 -*
from django import forms
from estagioapp.models.contato import Contato
from estagioapp.models.tipo_contato import TipoContato
from django.conf import settings


class FormContato(forms.Form):

    TIPO_CONTATO = 'tipo_contato_'
    CONTATO = 'contato_'

    def __init__(self, *args, **kwargs):
        super(FormContato, self).__init__(*args, **kwargs)

        dict_args = dict(*args)

        self.contato_keys = {}
        for i in range(settings.NR_CONTATOS):

            key_tipo_contato = self.TIPO_CONTATO+str(i)
            key_contato = self.CONTATO+str(i)

            tipo_contato_valor = ''
            contato_valor = ''

            if key_tipo_contato in dict_args:
                tipo_contato_valor = dict_args[key_tipo_contato][0]

            if key_contato in dict_args:
                contato_valor = dict_args[key_contato][0]

            self.fields[key_tipo_contato] = forms.ModelChoiceField(required=False, initial=tipo_contato_valor,
                widget=forms.Select(
                    attrs={'id': key_tipo_contato, 'name': key_tipo_contato, 'onchange':'config_contato("%s");' % (key_contato)}
                ),
                queryset=TipoContato.objects.all(),
                empty_label="Selecione um tipo de contato")

            if tipo_contato_valor != '' and int(tipo_contato_valor) == 1:
                self.fields[key_contato] = forms.EmailField(max_length=200, required=False, initial=contato_valor,
                                                           widget=forms.TextInput(
                                                               attrs={'id': key_contato,
                                                                      'name': key_contato,
                                                                      'placeholder': 'Preencha o contato'}))

            else:
                self.fields[key_contato] = forms.CharField(max_length=200, required=False, initial=contato_valor,
                                                           widget=forms.TextInput(
                                                               attrs={'id': key_contato,
                                                                      'name': key_contato,
                                                                      'placeholder': 'Preencha o contato'}))
            self.contato_keys[i] = [key_tipo_contato, key_contato]

    def get_contatos(self):

        contatos = []
        for i in range(settings.NR_CONTATOS):
            key_tipo_contato = self.TIPO_CONTATO+str(i)
            key_contato = self.CONTATO+str(i)

            tipo_contato = self.data[key_tipo_contato]
            valor_contato = self.data[key_contato]

            if tipo_contato == '' or valor_contato == '':
                continue

            contato = Contato()
            contato.tipo_contato_id = int(tipo_contato)
            contato.contato = valor_contato

            contatos.append(contato)

        return contatos

    def field_error(self, key):
        return self.errors[key]

    def save_contatos(self):
        contato_ids = []
        for contato in self.get_contatos():
            contato.save()
            contato_ids.append(contato.id)

        return contato_ids