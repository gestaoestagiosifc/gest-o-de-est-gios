#!/usr/bin/env python
# -*- coding: utf-8 -*

from django import forms
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Q
from estagioapp.models import Concedente, TipoConcedente, Convenio, LogConvenioStatus
from estagioapp.forms import FormPessoa, FormEndereco, FormContato
from django.forms.util import ErrorList


class FormSolicitacaoConvenio(FormPessoa, FormEndereco, FormContato):

    def __init__(self, id_convenio, readonly, *args, **kwargs):
        self.id_convenio = id_convenio
        self.readonly = readonly
        if self.id_convenio is None:
            super(FormSolicitacaoConvenio, self).__init__(*args, **kwargs)
        else:
            convenio = Convenio().get_convenio(id_convenio)
            self.status = convenio.get_status_atual()
            self.valido = True if self.status.status_id == settings.ST_CONVENIO_APROVADO else False
            self.anexo = convenio.anexo
            concedente = convenio.concedente
            endereco = concedente.endereco

            data = {
                'cnpj': concedente.cnpj,
                'numero_crea': concedente.crea,
                'crea': concedente.crea,
                'estado': endereco.cidade.estado,
                'nome_empresa': concedente.nome_empresa,
                'tipo_concedente': concedente.tipo_concedente,
                'area_atuacao': concedente.area_atuacao,
                'cpf': concedente.cpf,
                'nome_responsavel': concedente.first_name,
                'rua': endereco.rua,
                'numero': endereco.numero,
                'bairro': endereco.bairro,
                'referencia': endereco.referencia,
                'usuario': concedente.username,
                'cidade': endereco.cidade,
                'cep': endereco.cep
            }
            super(FormSolicitacaoConvenio, self).__init__(initial=data, *args, **kwargs)

            contatos = concedente.contato.all()
            for i in range(len(contatos)):
                key_tipo_contato = self.TIPO_CONTATO+str(i)
                key_contato = self.CONTATO+str(i)
                self.fields[key_tipo_contato].initial = contatos[i].tipo_contato_id
                self.fields[key_contato].initial = contatos[i]

        self.fields['crea'] = forms.CharField(required=False, widget=forms.HiddenInput(attrs={'id': 'crea'}))
        self.fields['numero_crea'] = forms.CharField(required=False, widget=forms.TextInput(attrs={'id': 'numero_crea', 'placeholder': '0000000'}))
        self.fields['cnpj'] = forms.CharField(required=False, widget=forms.TextInput(attrs={'id': 'cnpj', 'placeholder': '00.000.000/0000-00'}))
        self.fields['nome_empresa'] = forms.CharField(widget=forms.TextInput(attrs={'id': 'nome_empresa',  'placeholder': 'Nome da Empresa'}))
        self.fields['area_atuacao'] = forms.CharField(widget=forms.TextInput(attrs={'id': 'area_atuacao', 'placeholder': 'Área de atuação'}))
        self.fields['tipo_concedente'] = forms.ModelChoiceField(
            widget=forms.Select(attrs={'id': 'tipo_concedente'}), 
            queryset=TipoConcedente.objects.all())

        if readonly:
            for field in self.fields:
                if type(self.fields[field]) is forms.ModelChoiceField:
                    self.fields[field].widget.attrs['disabled'] = True
                else:
                    self.fields[field].widget.attrs['readonly'] = True

    def save_meu_convenio(self, id_convenio):
        dados = self.data
        convenio = Convenio().get_convenio(id_convenio)

        concedente = convenio.concedente
        concedente.username = dados['usuario']
        concedente.first_name = dados['nome_responsavel']
        concedente.tipo_concedente_id = dados['tipo_concedente']
        concedente.nome_empresa = dados['nome_empresa']
        concedente.area_atuacao = dados['area_atuacao']

        if concedente.cpf != dados['cpf']:
            concedente.cpf = dados['cpf']

        if 'cnpj' in dados:
            concedente.cnpj = dados['cnpj']
        if 'crea' in dados:
            concedente.crea = dados['crea']

        contatos = concedente.contato.all()
        contatos.delete()

        contato_ids = self.save_contatos()
        concedente.contato.add(*contato_ids)
        concedente.save()

        endereco = concedente.endereco
        endereco.rua = dados['rua']
        endereco.numero = dados['numero']
        endereco.bairro = dados['bairro']
        endereco.cep = dados['cep']
        endereco.cidade_id = dados['cidade']
        endereco.save()

    def clean(self):

        cleaned_data = self.cleaned_data

        if len(self.get_contatos()) == 0:
            self.erro_contatos = ErrorList(['Preencha pelo menos um contato'])
            self._errors[self.TIPO_CONTATO+'0'] = self.erro_contatos
            self._errors[self.CONTATO+'0'] = self.erro_contatos

        return cleaned_data

    def clean_tipo_concedente(self):

        tipoConcedente = self.cleaned_data['tipo_concedente']
        if tipoConcedente == "":
            raise forms.ValidationError('Campo Obrigatório')
        else:
            return tipoConcedente

    def clean_cnpj(self):
        cnpj = self.cleaned_data['cnpj']

        buscaCnpj = Convenio.objects.filter(~Q(id=self.id_convenio), Q(concedente__cnpj=cnpj))

        if buscaCnpj.count() and cnpj != "":
            raise forms.ValidationError('Cnpj já cadastrado')
        else:
            return cnpj

    def clean_crea(self):
        crea = self.cleaned_data['crea']
        buscaCrea = Convenio.objects.filter(~Q(id=self.id_convenio), Q(concedente__crea=crea))
        if buscaCrea.count() and crea != "":
            raise forms.ValidationError('Crea já cadastrado')
        else:
            return crea

    def clean_usuario(self):
        usuario = self.cleaned_data['usuario']
        convenio = Convenio.objects.filter(~Q(id=self.id_convenio), Q(concedente__username=usuario))
        if convenio.count():
            raise forms.ValidationError('Usuário já cadastrado')
        else:
            return usuario

    def clean_cpf(self):
        cpf = self.cleaned_data['cpf']
        buscaCpf = Convenio.objects.filter(~Q(id=self.id_convenio), Q(concedente__cpf=cpf))
        if buscaCpf.count():
            raise forms.ValidationError('Cpf já cadastrado')
        else:
            return cpf

    def criar_nova_solicitacao(self, usuario_id):

        dados = self.data
        # ==== Query para buscar o id do tipo de concedente, pois o self.cleaned_data só manda o valor
        tipo_concedente_id = self.data['tipo_concedente']

        endereco = self.save_endereco(self.id_convenio)

        if self.id_convenio:
            convenio = Convenio.objects.get(pk=self.id_convenio)
            concedente = convenio.concedente

        else:
            convenio = Convenio()
            concedente = Concedente()
            concedente.set_password(dados["senha"])
            concedente.username = dados["usuario"]
            concedente.first_name = dados['nome_responsavel']

        concedente.cnpj = dados["cnpj"]
        concedente.crea = dados["crea"]
        concedente.nome_empresa = dados["nome_empresa"]
        concedente.tipo_concedente_id = tipo_concedente_id
        concedente.area_atuacao = dados["area_atuacao"]

        concedente.cpf = dados["cpf"]
        concedente.endereco_id = endereco.id

        concedente.save()

        contato_ids = self.save_contatos()

        concedente.contato.add(*contato_ids)

        if self.id_convenio is None:
            User(concedente.id).groups.add(settings.PERM_GRUPO_EMPRESA)

        convenio.concedente = concedente

        convenio.save()

        log = LogConvenioStatus()

        log.status_id = settings.ST_CONVENIO_NAO_AVALIADO
        log.convenio_id = convenio.id
        #log.usuario_id = usuario_id

        log.save()

        return convenio
