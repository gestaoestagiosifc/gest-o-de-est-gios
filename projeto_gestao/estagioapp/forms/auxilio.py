# coding: utf-8

from django import forms
from django.db.models import Q
from estagioapp.models.auxilio import Auxilio
from estagioapp.models.vaga_auxilio import VagaAuxilio


class FormAuxilio(forms.Form):

    def __init__(self, id_vaga, *args, **kwargs):

        self.id_vaga = id_vaga

        super(FormAuxilio, self).__init__(*args, **kwargs)    
            
        self.fields['auxilio'] = forms.ModelChoiceField(
                widget=forms.Select(),
                queryset=Auxilio.objects.all(),
                empty_label="Selecione um auxílio"
        )
        self.fields['valor'] = forms.DecimalField(min_value=0, max_digits=10, decimal_places=2, widget=forms.TextInput(attrs={'placeholder': '00,00'}))


    def clean_auxilio(self):
        tipo_auxilio = self.cleaned_data["auxilio"]
        busca_tipos_auxilio = VagaAuxilio.objects.filter(Q(vaga=self.id_vaga))
        for tipo in busca_tipos_auxilio:
            if tipo_auxilio is not None:
                if tipo_auxilio.id == tipo.auxilio_id:
                    raise forms.ValidationError('Tipo de Auxilio já cadastrado')
        return tipo_auxilio

    def save_auxilio(self, id_vaga):
        dados = self.cleaned_data

        auxilio_dados = VagaAuxilio(
                        valor = dados['valor'],
                        auxilio_id = self.data["auxilio"],
                        vaga_id = id_vaga
        )
        auxilio_dados.save()
        return None
