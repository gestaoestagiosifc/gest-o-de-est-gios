# coding: utf-8

from django import forms
from django.conf import settings
from estagioapp.models import LogVagaStatus, Vaga, Funcao, Turno


class FormVaga(forms.Form):
    hora_formato = ["%H:%M"]

    descricao_vaga = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Descrição da Vaga'}))
    hora_fim = forms.TimeField(input_formats=hora_formato, widget=forms.TextInput(attrs={'placeholder':'hh:mm'}))
    hora_inicio = forms.TimeField(input_formats=hora_formato, widget=forms.TextInput(attrs={'placeholder': 'hh:mm'}))
    turno = forms.ModelChoiceField(queryset=Turno.objects.all())
    funcao = forms.ModelChoiceField(queryset=Funcao.objects.all())
    quantidade_vaga = forms.IntegerField(min_value=1, widget=forms.TextInput(attrs={'placeholder': 'Apenas valores positivos'}))


    def clean_hora_inicio(self):

        if 'hora_fim' in self.cleaned_data:
            hora_inicio = self.cleaned_data['hora_inicio']
            hora_fim = self.cleaned_data['hora_fim']

            if hora_inicio >= hora_fim:
                raise forms.ValidationError('A hora de entrada deve ser menor que a hora de saída')

            return hora_inicio
        else:
            raise forms.ValidationError('Preencha o campo do horário de saída')


    def save_vaga(self, id_oferta):

        dados = self.cleaned_data

        for vaga in range(int(dados["quantidade_vaga"])):
            dados_vaga = Vaga(
                descricao=dados["descricao_vaga"],
                hora_inicio=dados["hora_inicio"],
                hora_fim=dados["hora_fim"],
                turno_id=self.data["turno"],
                funcao_id=self.data["funcao"],
                oferta_id=id_oferta)
            dados_vaga.save()

            log = LogVagaStatus()
            log.vaga_id = dados_vaga.id
            log.status_id = settings.ST_VAGA_NAO_PREENCHIDA
            log.save()
