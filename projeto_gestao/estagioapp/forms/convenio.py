#coding: utf-8
from django import forms


class FormAvaliacaoConvenio(forms.Form):
    anexo = forms.FileField(required=False)

    def clean_anexo(self):
        anexo = self.cleaned_data['anexo']

        if anexo is not None and '.' in anexo.name:
            extensao = anexo.name.split('.')[1]

            if extensao not in ['pdf']:
                raise forms.ValidationError('Extensão inválida!')

        return anexo
