# coding: utf-8

from django import forms
from estagioapp.models.oferta import Oferta
import datetime


class FormOferta(forms.Form):
    titulo = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'placeholder': 'Título da Oferta'}))
    descricao = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Descrição da Oferta'}))
    data_vencimento = forms.DateField(input_formats=["%d/%m/%Y"], widget=forms.DateInput(attrs={'placeholder': 'dd/mm/aaaa'}))
    
    CHOICES = ((True, 'Sim'), (False, 'Não'))

    liberar = forms.ChoiceField(initial=False, choices=CHOICES, widget=forms.RadioSelect(attrs={'id':'liberar'}))

    def clean_data_vencimento(self):
        dt_vencimento = self.cleaned_data["data_vencimento"]
        dt_atual = datetime.date.today()

        if dt_vencimento <= dt_atual:
            raise forms.ValidationError('Data de vencimento deve ser maior que a data atual')
        else:
            return dt_vencimento

    def save_oferta(self, id_concedente):
        dados = self.cleaned_data
        dados['liberar'] = True if dados['liberar'] == 'True' else False
        dados_oferta = Oferta(
               titulo = dados["titulo"],
               descricao = dados["descricao"],
               data_vencimento = dados["data_vencimento"],
               data = datetime.date.today(),
               liberado = dados['liberar'],
               concedente_id = id_concedente
        )
        dados_oferta.save()
        return dados_oferta.id