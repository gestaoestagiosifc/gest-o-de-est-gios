# coding: utf-8
from django.views.generic.base import View
from django.shortcuts import HttpResponse
from django.template.loader import render_to_string
from decorators.permissoes import group_required
from django.conf import settings
from django.utils.decorators import method_decorator
from estagioapp.models import Estagio
from datetime import date
from latex import LatexDocument


class ImprimirTermoView(View):

    def get(self, request, id):

        estagio = Estagio()
        #Renderiza o template com extensão .tex
        latex = render_to_string('estagio/imprimir_termo.tex', {'estagio': estagio, 'data': date.today()})

        #Transdorma em documento latex
        tex = LatexDocument(latex.encode('utf-8'))

        #Adiciona as imagens do documento
        tex.add_file(settings.BASE_DIR + "/../static/images/logo-ifc2.png", "logo-ifc2.png")
        #Transforma em pdf
        pdf = tex.as_pdf()

        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s.pdf' % 'convenio'

        # abre o pdf na propria aba
        # return HttpResponse(response, mimetype='application/pdf')
        # baixar o arquivo direto
        return response
