# coding: utf-8

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.views.generic.base import View
from django.conf import settings
from django.utils.decorators import method_decorator
from estagioapp.models import Vaga, Candidatura, LogVagaStatus
from estagioapp.forms.vaga import FormVaga
from decorators.permissoes import group_required


class DetalheVagaView(View):
    template = 'oferta/detalhe_vaga.html'

    @method_decorator(group_required(settings.PERM_GRUPO_ALUNO, settings.PERM_GRUPO_COORDENADOR, settings.PERM_GRUPO_EMPRESA))
    def get(self, request, id_vaga):
        vaga = Vaga.objects.get(pk=id_vaga)
        candidatura = Candidatura.objects.filter(aluno_id=request.user.id, vaga_id=id_vaga)
        
        vaga.verifica_candidatura = len(candidatura) > 0

        return render(request, self.template, {
            'vaga':vaga
        })

class CandidatarVagaView(View):

    @method_decorator(group_required(settings.PERM_GRUPO_ALUNO))
    def get(self, request, id_vaga):

        candidatura = Candidatura()
        candidatura.vaga_id = id_vaga
        candidatura.aluno_id = request.user.id
        candidatura.save()

        messages.success(request, 'Candidatura efetuada com sucesso!')
        return HttpResponseRedirect('/listar-ofertas/')

class CadastrarVagaView(View):

    template = 'oferta/add_vaga.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request, id_oferta):
        form = FormVaga()
        return render(request, self.template, {
            'form_vaga': form,
            'id_oferta': id_oferta
        })

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def post(self, request, id_oferta):
        form = FormVaga(request.POST)
        if form.is_valid():
            form.save_vaga(id_oferta)
            messages.success(request, request.POST['quantidade_vaga']+' vaga(s) adicionada(s) com sucesso!')
            return HttpResponseRedirect('/editar-oferta/'+str(id_oferta))

        return render(request, self.template, {
            'form_vaga': form,
            'id_oferta': id_oferta
        })


class EditarVagaView(View):

    template = 'oferta/editar_vaga.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request, id_vaga):

        vaga = Vaga.objects.get(pk=id_vaga)

        data = {'funcao':vaga.funcao,
                'descricao_vaga': vaga.descricao,
                'turno': vaga.turno,
                'hora_inicio': vaga.hora_inicio.strftime("%H:%M"),
                'hora_fim': vaga.hora_fim.strftime("%H:%M")
        }

        form = FormVaga(initial=data)
        return render(request, self.template, {
            'form_vaga':form,
            'id_vaga':vaga.id,
            'id_oferta':vaga.oferta_id
        })

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def post(self, request, id_vaga):

        vaga = Vaga.objects.get(pk=id_vaga)
        form = FormVaga(request.POST)
        form.fields['quantidade_vaga'].required = False

        if form.is_valid():
            vaga.funcao_id = request.POST['funcao']
            vaga.turno_id = request.POST['turno']
            vaga.hora_inicio = request.POST['hora_inicio']
            vaga.hora_fim = request.POST['hora_fim']
            vaga.descricao = request.POST['descricao_vaga']
            vaga.save()
            messages.success(request, 'Vaga editada com sucesso!')
            return HttpResponseRedirect('/editar-oferta/'+str(vaga.oferta_id))

        return render(request, self.template, {
            'form_vaga': form,
            'id_vaga': vaga.id,
            'id_oferta':vaga.oferta_id
        })


class ExcluirVagaView(View):

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request, id_vaga):

        vaga = Vaga.objects.get(pk=id_vaga)

        if vaga.id is not None:
            log = LogVagaStatus()
            log.status_id = settings.ST_VAGA_CANCELADA
            log.vaga_id = id_vaga
            log.save()
            messages.success(request, 'Vaga cancelada com sucesso!')
            return HttpResponseRedirect('/editar-oferta/'+str(vaga.oferta_id))



class MinhasVagasView(View):
    template = 'oferta/minhas_vagas.html'

    def __page(self, request):
        procurar = ''
        classificar = 0

        if request.method == 'POST':

            if 'procurar' in request.POST:
                procurar = request.POST['procurar']

            if 'classificar' in request.POST:
                classificar = request.POST['classificar']

        else:

            if 'procurar' in request.GET:
                procurar = request.GET['procurar']

            if 'classificar' in request.GET:
                classificar = request.GET['classificar']

        try:
            page = int(request.GET.get('page', 1))
        except Exception:
            page = 1

        vagas = Candidatura().consulta_vagas_do_aluno(request.user.id, page, procurar, classificar)

        return render(request, self.template, {
            'vagas': vagas,
            'classificar': classificar,
            'procurar': procurar
        })

    @method_decorator(group_required(settings.PERM_GRUPO_ALUNO, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request):
        return self.__page(request)

    @method_decorator(group_required(settings.PERM_GRUPO_ALUNO, settings.PERM_GRUPO_COORDENADOR))
    def post(self, request):
        return self.__page(request)    



