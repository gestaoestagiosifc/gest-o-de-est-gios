# coding: utf-8

from django.shortcuts import render
from django.views.generic.base import View
from django.conf import settings
from django.utils.decorators import method_decorator
from estagioapp.models import Concedente
from decorators.permissoes import group_required


class ConsultaConcedenteView(View):
	
	template = 'oferta/consulta_empresas.html'

	@method_decorator(group_required(settings.PERM_GRUPO_COORDENADOR))
	def get(self, request):
		
		concedentes = Concedente.objects.all()

		return render(request, self.template, {
			'empresas': concedentes
		})
