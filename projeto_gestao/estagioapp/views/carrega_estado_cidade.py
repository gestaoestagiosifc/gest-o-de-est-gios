# coding: utf-8

from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from estagioapp.models import Cidade


@csrf_exempt
def carrega_estado_cidade(request):

    cidades = Cidade.objects.filter(estado_id=request.POST['estado']).order_by('nome');
    cidades_json = serializers.serialize('json', cidades)
    return HttpResponse(cidades_json, mimetype='application/json')



