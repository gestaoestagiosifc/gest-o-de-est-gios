#coding:utf-8

from django.views.generic.base import View
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from estagioapp.models.vaga import Vaga
from estagioapp.models.vaga_auxilio import VagaAuxilio
from estagioapp.forms.auxilio import FormAuxilio


class InserirAuxilioView(View):

    template = 'oferta/add_auxilio.html'

    def get(self, request, id_vaga):
        vaga = Vaga.objects.get(id=id_vaga)
        auxilios = VagaAuxilio.objects.filter(vaga=id_vaga)
        form = FormAuxilio(id_vaga)

        return render(request, self.template, {
            'vaga': vaga,
            'form': form,
            'auxilios': auxilios
        })

    def post(self, request, id_vaga):
        form = FormAuxilio(id_vaga, request.POST)
        auxilios = VagaAuxilio.objects.filter(vaga=id_vaga)
        vaga = Vaga.objects.get(id=id_vaga)

        if form.is_valid():
            form.save_auxilio(id_vaga)

        return render(request, self.template, {
            'vaga': vaga, 
            'form': form, 
            'auxilios': auxilios
        })     


class ListarAuxiliosView(View):

    template = 'oferta/listar_auxilios.html'

    def get(self, request, id_vaga):
        vaga = Vaga.objects.get(pk=id_vaga)
        
        return render(request, self.template, {
            'auxilios': vaga.get_auxilios,
            'id_vaga': id_vaga
        })


class EditarAuxilioView(View):

    template = 'oferta/editar_auxilio.html'

    def get(self, request, pk):
        
        auxilio = VagaAuxilio.objects.get(pk=pk)
        data = {
            'auxilio': auxilio.auxilio,
            'valor': auxilio.valor
        }

        form = FormAuxilio(auxilio.vaga_id, initial=data)
        return render(request, self.template,{
            'form':form,
            'pk': auxilio.id,
            'vaga': auxilio.vaga
        })

    def post(self, request, pk):
       
        auxilio = VagaAuxilio.objects.get(pk=pk)
 
        form = FormAuxilio(auxilio.vaga_id, request.POST)
        form.fields['auxilio'].required = False
        
        if form.is_valid():

            auxilio.valor = request.POST['valor']
            auxilio.save()

            return HttpResponseRedirect('/auxilios/'+str(auxilio.vaga_id))

        return render(request, self.template, {
            'form':form,
            'pk': auxilio.id,
             'vaga': auxilio.vaga
        })

class ExcluirAuxilioView(View):

    def get(self, request, pk):
        auxilio = VagaAuxilio.objects.get(pk=pk)
        if auxilio.id is not None:
            auxilio.delete()
            messages.success(request, 'Auxílio excluído com sucesso!')
            return HttpResponseRedirect('/auxilios/'+str(auxilio.vaga_id))