# coding: utf-8
from django.views.generic.base import View
from django.shortcuts import render, redirect, HttpResponse, RequestContext
from django.template.loader import render_to_string
from decorators.permissoes import group_required
from django.conf import settings
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.db import transaction
from estagioapp.models import Convenio, LogConvenioStatus, Estado
from estagioapp.forms import FormSolicitacaoConvenio, FormAvaliacaoConvenio
from datetime import date
from latex import LatexDocument
from StringIO import StringIO
import mimetypes
import os


class SolicitacaoConvenioView(View):
    template = 'convenio/solicitacao.html'

    def get(self, request, id=None):
        form = FormSolicitacaoConvenio(id, False)
        estados = Estado.objects.values('abreviado')
        return render(request, self.template, {'form': form, 'estados': estados})

    @method_decorator(transaction.commit_on_success)
    def post(self, request, id=None):
        estados = Estado.objects.values('abreviado')
        form = FormSolicitacaoConvenio(id, False, request.POST)

        if request.POST['tipo_concedente'] != '':
            if int(request.POST['tipo_concedente']) == 3:
                form.fields['numero_crea'].required = True
            else:
                form.fields['cnpj'].required = True

        if form.is_valid():
            form.criar_nova_solicitacao(request.user.id)

            return redirect('/')
        return render(request, self.template, {'form': form, 'estados': estados})


def remover_img(file):
    os.system("rm "+settings.MEDIA_ROOT+'/'+file)


class AvaliacaoConvenioView(View):

    template_name = 'convenio/validar.html'

    @method_decorator(group_required((settings.PERM_GRUPO_REITORIA)))
    def post(self, request, id):

        form = FormAvaliacaoConvenio(request.POST, request.FILES)
        anexo = None
        if form.is_valid():
            anexo = form.cleaned_data['anexo']

            convenio = Convenio.objects.get(pk=id)

            if convenio.anexo:
                remover_img(str(convenio.anexo))

            convenio.anexo = anexo
            convenio.save()


            log = LogConvenioStatus()
            log.convenio_id = id
            log.status_id = request.POST['status_convenio']
            log.observacao = request.POST['obs']
            log.usuario_id = request.user.id
            log.save()
            return HttpResponseRedirect('/home/')

        return render(request, self.template_name, {
            'form': FormSolicitacaoConvenio(id, True),
            'id': id, 'erro_extensao': form['anexo'].errors
        })

    @method_decorator(group_required(settings.PERM_GRUPO_REITORIA))
    def get(self, request, id):

        return render(request, self.template_name, {
            'form': FormSolicitacaoConvenio(id, True),
            'id': id
        })


class DownloadConvenioView(View):

    @method_decorator(group_required(settings.PERM_GRUPO_REITORIA))
    def get(self, request, id):

        convenio = Convenio.objects.get(pk=id)

        response = HttpResponse(convenio.anexo, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s.pdf' % 'convenio'

        return response


class MeuConvenioView(View):
    template_name = 'convenio/meu-convenio.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA))
    def get(self, request):

        convenio = Convenio.objects.filter(concedente_id=request.user.id).first()
        estados = Estado.objects.values('abreviado')

        log = convenio.get_status_atual()
        
        readonly = True if log.status_id == settings.ST_CONVENIO_APROVADO else False
        
        return render(request, self.template_name, {
            'form': FormSolicitacaoConvenio(convenio.id, readonly),
            'id': id,
            'estados': estados
        })

    @method_decorator(transaction.commit_on_success)
    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA))
    def post(self, request):

        convenio = Convenio.objects.filter(concedente_id=request.user.id).values('id').first()
        estados = Estado.objects.values('abreviado')
        form = FormSolicitacaoConvenio(convenio['id'], False, request.POST)
        form.fields['confirma_senha'].required = False
        form.fields['senha'].required = False

        if form.is_valid():
            form.save_meu_convenio(convenio['id'])
            return HttpResponseRedirect('/home/')
        else:
            print 'errors form : ',form.errors

        return render(request, self.template_name, {
            'form': form,
            'estados': estados
        })


class ConsultaConveniosView(View):

    template_name = 'convenio/convenios_reitoria.html'

    def __page(self, request):
        procurar = None
        classificacao = None

        if request.method == 'POST':

            if 'procurar' in request.POST:
                procurar = request.POST['procurar']

            if 'classificacao' in request.POST:
                classificacao = request.POST['classificacao']

        else:

            if 'valor' in request.GET:
                procurar = request.GET['procurar']

            if 'classificacao' in request.GET:
                classificacao = request.POST['classificacao']

        try:
            page = int(request.GET.get('page', 1))
        except Exception:
            page = 1

        convenios_page = Convenio().get_page(page, procurar, classificacao)

        return render(request, self.template_name, {'convenios': convenios_page})

    @method_decorator(group_required(settings.PERM_GRUPO_REITORIA))
    def get(self, request):

        return self.__page(request)

    @method_decorator(group_required(settings.PERM_GRUPO_REITORIA))
    def post(self, request):

        return self.__page(request)


'''
Para executar a impressão, necessario instalar
sudo apt-get install texlive-latex-base texlive-latex-extra texlive-latex-recommended

pip install django-latex

Referencia do django-latex
https://gitorious.org/si-fai/si-fai/commit/b7fefb54f9e97b20ec75b10fe4cf1427cf1cce69#src/si/views/adherent.py
'''
class ImprimirConvenioView(View):

    @method_decorator(group_required(settings.PERM_GRUPO_REITORIA))
    def get(self, request, id):

        convenio = Convenio.objects.get(pk=id)
        #Renderiza o template com extensão .tex
        latex = render_to_string('convenio/imprimir_convenio.tex', {'convenio': convenio, 'data': date.today()})

        #Transdorma em documento latex
        tex = LatexDocument(latex.encode('utf-8'))

        #Adiciona as imagens do documento
        tex.add_file(settings.BASE_DIR + "/../static/images/logo_minis.png", "logo_minis.png")
        tex.add_file(settings.BASE_DIR + "/../static/images/logo_reitoria.jpg", "logo_reitoria.jpg")
        tex.add_file(settings.BASE_DIR + "/../static/images/logo_endereco.jpg", "logo_endereco.jpg")
        #Transforma em pedf
        pdf = tex.as_pdf()

        response = HttpResponse(pdf, mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s.pdf' % 'convenio'

        # abre o pdf na propria aba
        # return HttpResponse(response, mimetype='application/pdf')
        # baixar o arquivo direto
        return response
