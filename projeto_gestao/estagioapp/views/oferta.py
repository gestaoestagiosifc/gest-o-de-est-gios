# coding: utf-8

from django.shortcuts import render
from django.views.generic.base import View
from django.conf import settings
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib import messages
from estagioapp.forms.vaga import FormVaga
from estagioapp.forms.oferta import FormOferta
from estagioapp.models import Oferta, Vaga, Concedente
from decorators.permissoes import group_required
import datetime


class CadastrarOfertaView(View):

    '''  Esta classe faz o cadastro das ofertas e vagas com permissão para a empresa '''

    template = 'oferta/cadastro_oferta.html'
    id_oferta = None

    ''' O dicionário vagas irá receber as vagas cadastrada em determinada oferta,
        como ainda não tem nada, então ele inicia em branco'''

    vagas = {}

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA))  # decorator para permissão da empresa
    def get(self, request):
        '''
        Função para o primeiro acesso ao formulário de cadastro de vagas e ofertas por GET
        '''
        permitir_adicionar_oferta = Concedente().permitir_adicionar_oferta(request.user.id)

        if permitir_adicionar_oferta:

            form_vaga = FormVaga()
            form_oferta = FormOferta()

            # Try para casos de erro da variável ainda não existir e Multi value Error.
            try:
                ''' Esta verificação serve para sabermos se retornou da tela de auxílios com o id da oferta '''

                if request.GET["oferta"] is not None:
                    self.id_oferta = request.GET["oferta"]
                    self.vagas = Vaga.objects.filter(oferta=self.id_oferta)
            except:
                pass

            return render(request, self.template, {
                'form_vaga': form_vaga,
                'form_oferta': form_oferta,
                'id_oferta': self.id_oferta,
                'vagas': self.vagas
            })

        else:

            messages.error(request, 'Seu convênio ainda não foi aprovado')
            return HttpResponseRedirect('/home/')


    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA))  # decorator para permissão da empresa
    def post(self, request):
        ''' Função que irá receber o formulário de oferta e vaga, via POST'''
        form_vaga = FormVaga(request.POST)
        # ======= Query para retornar o concedente que está logado
        concedente = Concedente.objects.get(id=request.user.id)

        # ======= Se a oferta ainda não tiver nenhuma vaga cadastrada
        if len(self.vagas) == 0:
            form_oferta = FormOferta(request.POST)
        else:
            form_oferta = None

        #======== Se os formulário de vaga estiver correto
        if form_vaga.is_valid():

            #===== Se o formulário de oferta estiver correto e não for nulo
            if form_oferta is not None and form_oferta.is_valid():

                self.id_oferta = form_oferta.save_oferta(concedente.id)
            else:
                # Pegar o ID da oferta já cadastrada, no campo Hidden
                self.id_oferta = request.POST["id_oferta"]


            # Salvando a vaga
            form_vaga.save_vaga(self.id_oferta)

            # == Query com as vagas cadastradas na oferta
            self.vagas = Vaga.objects.filter(oferta=self.id_oferta)

        return render(request, self.template, {
            'form_vaga': form_vaga,
            'form_oferta': form_oferta,
            'id_oferta': self.id_oferta,
            'vagas': self.vagas
        })


class EditarOfertaView(View):

    template = 'oferta/editar_oferta.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request, id_oferta):

        permitir_adicionar_oferta = Concedente().permitir_adicionar_oferta(request.user.id)

        if permitir_adicionar_oferta:

            oferta = Oferta.objects.get(pk=id_oferta)

            data = {'titulo':oferta.titulo, 
                    'data_vencimento':oferta.data_vencimento,
                    'descricao':oferta.descricao,
                    'liberar':oferta.liberado}
            
            form_oferta = FormOferta(initial=data)
            
            return render(request, self.template, {
                'form_oferta': form_oferta,
                'id_oferta': id_oferta,
                'vagas':oferta.get_vagas_editar()
            })

        else:
            
            messages.error(request, 'Seu convênio ainda não foi aprovado')
            return HttpResponseRedirect('/home/')

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def post(self, request, id_oferta):

        oferta = Oferta.objects.get(pk=id_oferta)

        form = FormOferta(request.POST)
        if form.is_valid():

            data = datetime.datetime.strptime(request.POST['data_vencimento'], "%d/%m/%Y")
            oferta.titulo = request.POST['titulo']
            oferta.data_vencimento = data
            oferta.descricao = request.POST['descricao']
            oferta.liberado = True if request.POST['liberar'] == 'True' else False
            oferta.save()
            return HttpResponseRedirect('/consultar-ofertas/')

        return render(request, self.template, {
            'form_oferta':form,
            'id_oferta':oferta.id,
            'vagas':oferta.get_vagas_editar()
        })


class ConsultarOfertasEmpresaView(View):

    template = 'oferta/empresa_listar_ofertas.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA))
    def get(self, request):

        ofertas = Oferta.objects.filter(concedente_id=request.user.id)

        return render(request, self.template, {
            'ofertas': ofertas
        })


class ListarOfertaView(View):
    template = 'oferta/listar_ofertas.html'

    def __page(self, request):
        procurar = ''
        classificar = 0

        if request.method == 'POST':

            if 'procurar' in request.POST:
                procurar = request.POST['procurar']

            if 'classificar' in request.POST:
                classificar = request.POST['classificar']

        else:

            if 'procurar' in request.GET:
                procurar = request.GET['procurar']

            if 'classificar' in request.GET:
                classificar = request.GET['classificar']

        try:
            page = int(request.GET.get('page', 1))
        except Exception:
            page = 1

        ofertas_page = Oferta().consulta_aluno_page(page, procurar, classificar)

        return render(request, self.template, {
            'ofertas': ofertas_page,
            'procurar': procurar,
            'classificar': classificar
        })

    @method_decorator(group_required(settings.PERM_GRUPO_ALUNO, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request):

        return self.__page(request)

    @method_decorator(group_required(settings.PERM_GRUPO_ALUNO, settings.PERM_GRUPO_COORDENADOR))
    def post(self, request):

        return self.__page(request)


class DetalheOfertaView(View):
    template = 'oferta/detalhe_oferta.html'

    @method_decorator(group_required(settings.PERM_GRUPO_ALUNO, settings.PERM_GRUPO_COORDENADOR, settings.PERM_GRUPO_EMPRESA))
    def get(self, request, id_oferta):
        oferta = Oferta.objects.get(pk=id_oferta)

        grupos = request.user.groups.values_list('id', flat=True).all()
        vagas = None
        if settings.PERM_GRUPO_EMPRESA in grupos:
            vagas = Vaga.objects.filter(oferta_id=id_oferta).all()
        else:
            vagas = oferta.get_vagas()

        return render(request, self.template, {
            'oferta': oferta,'vagas' : vagas
        })
