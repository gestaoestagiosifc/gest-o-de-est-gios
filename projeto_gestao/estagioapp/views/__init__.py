# coding: utf-8

# Convênio
from convenio import AvaliacaoConvenioView
from convenio import ConsultaConveniosView
from convenio import MeuConvenioView
from convenio import SolicitacaoConvenioView
from convenio import ImprimirConvenioView
from convenio import DownloadConvenioView

# Ofertas
from oferta import CadastrarOfertaView
from oferta import EditarOfertaView
from oferta import ListarOfertaView
from oferta import DetalheOfertaView
from oferta import ConsultarOfertasEmpresaView

# Vagas
from vaga import DetalheVagaView
from vaga import CandidatarVagaView
from vaga import MinhasVagasView
from vaga import CadastrarVagaView
from vaga import EditarVagaView
from vaga import ExcluirVagaView

# Auxílio
from auxilio import InserirAuxilioView
from auxilio import ListarAuxiliosView
from auxilio import EditarAuxilioView
from auxilio import ExcluirAuxilioView


# Candidatos
from candidatos import CandidatosView
from candidatos import DetalheCandidatoView
from candidatos import ContratarCandidatoView
from candidatos import CandidatosPorVagaView

# Empresas
from concedente import ConsultaConcedenteView

# Estagio
from estagio import ImprimirTermoView
