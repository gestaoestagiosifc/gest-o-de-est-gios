# coding: utf-8

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.conf import settings
from django.utils.decorators import method_decorator
from estagioapp.models import Candidatura, LogVagaStatus
from decorators.permissoes import group_required


class CandidatosView(View):

    template = 'oferta/empresa_visualizar_candidatos.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request):
        
        candidatos = Candidatura().get_candidaturas_nao_preenchidas(request.user.id)

        return render(request, self.template, {
            'candidatos': candidatos,
        })


class DetalheCandidatoView(View):

    template = 'oferta/detalhe_candidato.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request, id_candidatura):
        
        candidatura = Candidatura.objects.get(pk=id_candidatura)
        return render(request, self.template, {
            'candidatura': candidatura,
        })


class ContratarCandidatoView(View):

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request, id_vaga):
        log = LogVagaStatus()
        log.vaga_id = id_vaga
        log.status_id = settings.ST_VAGA_PREECHIDA
        log.save()
        return HttpResponseRedirect('/candidatos/')

class CandidatosPorVagaView(View):

    template = 'oferta/candidatos_por_vaga.html'

    @method_decorator(group_required(settings.PERM_GRUPO_EMPRESA, settings.PERM_GRUPO_COORDENADOR))
    def get(self, request, id_vaga):

        candidatos = Candidatura().get_candidatos_por_vaga(id_vaga)

        return render(request, self.template, {
            'candidatos': candidatos
        })

