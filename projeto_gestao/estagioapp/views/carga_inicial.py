# coding:utf-8
from django.shortcuts import redirect, render, HttpResponse
from django.contrib.auth.models import Group, User
from django.db import transaction
from estagioapp.models import Orientador, Seguradora, Campus, Endereco, Pais, Estado, Cidade, Aluno, Curso, \
    StatusConvenio, StatusVaga, TipoConcedente, TipoContato
from django.conf import settings
from django.contrib.auth.models import User
import psycopg2


# Metodo utilizado para corrigir o numero de caracteres, o banco de SIGA-EDU esta sem o zero na frente do cpf.
def corrigi_cpf(cpf):
    if len(cpf) < 11:
        count = 11 - len(cpf)
        for i in range(count):
            cpf = '0' + cpf
    return cpf


@transaction.commit_on_success
def carga(request):

    grupos = ['Aluno', 'Orientador', 'Coordenador', 'Reitoria', 'Empresa', 'Supervisor']

    for gp_nome in grupos:
        gp = Group.objects.filter(name=gp_nome)

        if len(gp) == 0:
            gp = Group()
            gp.name = gp_nome
            gp.save()

    status_convenios_descricao = [('Não avaliado', 'st_nao_avaliado'), ('Em andamento', 'st_em_andamento'),
                                  ('Aprovado', 'st_aprovado'), ('Recusado', 'st_recusado'),
                                  ('Cancelado', 'st_cancelado')]

    for st in status_convenios_descricao:

        status = StatusConvenio.objects.filter(descricao=st[0])

        if len(status) == 0:
            status = StatusConvenio()
        else:
            status = status.first()
        status.descricao = st[0]
        status.classe_css = st[1]

        status.save()

    status_vagas = [('Não preenchida','status_vaga2'), ('Preenchida', 'status_vaga1'),
                              ('Cancelada', 'status_vaga3')]

    for st_vaga in status_vagas:

        status = StatusVaga.objects.filter(descricao=st_vaga[0])

        if len(status) == 0:
            status = StatusVaga()
            status.descricao = st_vaga[0]
            status.classe_css = st_vaga[1]

            status.save()

    tipos_concedente = ['Público', 'Privado', 'Profissional liberal']

    for tp in tipos_concedente:
        tipo_concedente = TipoConcedente.objects.filter(descricao=tp)

        if len(tipo_concedente) == 0:
            tipo_concedente = TipoConcedente()
            tipo_concedente.descricao = tp
            tipo_concedente.save()

    tipos_contatos = ['E-mail', 'Telefone', 'Celular']

    for tp in tipos_contatos:
        tipo_contato = TipoContato.objects.filter(descricao=tp)

        if len(tipo_contato) == 0:
            tipo_contato = TipoContato()
            tipo_contato.descricao = tp
            tipo_contato.save()

    gestaodb = settings.DATABASES['default']
    db = psycopg2.connect(database=gestaodb['NAME'], user=gestaodb['USER'], password=gestaodb['PASSWORD'],
                          host=gestaodb['HOST'], port=gestaodb['PORT'])

    cursor = db.cursor()

    cursor.execute('alter table auth_user alter column first_name TYPE character varying(200)')
    cursor.execute('alter table auth_user alter column username TYPE character varying(200)')
    cursor.execute('alter table auth_user alter column email TYPE character varying(200)')

    db.commit()
    db.close()

    return redirect('/')


@transaction.commit_on_success
def importa_dados_sigaedu(request):

    gestaodb = settings.DATABASES['default']
    db = psycopg2.connect(database="sigaedu", user=gestaodb['USER'], password=gestaodb['PASSWORD'],
                          host=gestaodb['HOST'], port=gestaodb['PORT'])

    cursor = db.cursor()

    #Pais
    cursor.execute('''select id, nome from pais ''')
    paises_siga = cursor.fetchall()

    for pais_siga in paises_siga:

        pais = Pais.objects.filter(id=pais_siga[0])

        if len(pais) > 0:
            continue

        pais = Pais()
        pais.id = pais_siga[0]
        pais.nome = pais_siga[1]
        pais.save()

    #Estado
    cursor.execute('''select id, descricao as nome, sigla as abreviado, pais_id from unidade_federativa ''')
    estados_siga = cursor.fetchall()

    for estado_siga in estados_siga:
        estado = Estado.objects.filter(id=estado_siga[0])

        if len(estado) > 0:
            continue

        estado = Estado()

        estado.id = estado_siga[0]
        estado.nome = estado_siga[1]
        estado.abreviado = estado_siga[2]
        estado.pais_id = estado_siga[3]
        estado.save()

    #Cidade
    cursor.execute('''select id, nome, unidade_federativa_id as estado_id from municipio ''')
    cidades_siga = cursor.fetchall()

    for cidade_siga in cidades_siga:
        cidade = Cidade.objects.filter(id=cidade_siga[0])

        if len(cidade) > 0:
            continue

        cidade = Cidade()

        cidade.id = cidade_siga[0]
        cidade.nome = cidade_siga[1]
        cidade.estado_id = cidade_siga[2]
        cidade.save()


    #Seguradora
    segu_cnpj = '66305057000107'
    segu = Seguradora.objects.filter(cnpj=segu_cnpj)

    if len(segu) == 0:
        segu = Seguradora()
        segu.cnpj = segu_cnpj
        segu.nome = 'Seguradora teste'
        segu.email = 'seguradora@teste.com'
        segu.save()

    else:
        segu = segu.get()

    #Campus

    campus_cnpj = ['33566788000157', '22754041000106']
    #Campus Araquari e São Francisco
    cursor.execute('''select nome from elemento_organizacional where id in (33,52) ''')
    campus_siga = cursor.fetchall()

    endereco = Endereco.objects.first()

    if endereco is None:
        endereco = Endereco()
        endereco.cidade_id = 4395
        endereco.bairro = 'Porto Grande'
        endereco.cep = 89245000
        endereco.rua = 'Rodovia BR 280'
        endereco.numero = 'km 27 - cx 21'

        endereco.save()

    for cp in campus_siga:

        cnpj = campus_cnpj[0]
        campus = Campus.objects.filter(cnpj=cnpj)
        if len(campus) > 0:
            campus_cnpj.remove(cnpj)
            continue

        campus = Campus()

        campus.cnpj = cnpj
        campus.nome = cp[0]
        campus.endereco_id = endereco.id
        campus.seguradora_id = segu.id
        campus.save()

        campus_cnpj.remove(cnpj)

    campus_cnpj = ['33566788000157', '22754041000106']

    #Cursos de Araquari
    cursor.execute('''
select d.id as id, d.nome as nome,d.finalidade as finalidade,d.nome_reduzido as abreviado,
d.tempo_maximo as tempo_maximo, d.tempo_minimo as tempo_minimo, i.id as campus_id
from curso d inner join unidade_organizacional e on (d.unidade_organizacional_id=e.elemento_organizacional_id)
inner join elemento_organizacional f on (e.elemento_organizacional_id=f.id)
inner join elemento_organizacional g on (f.elemento_organizacional_id=g.id)
inner join elemento_organizacional h on (g.elemento_organizacional_id=h.id)
inner join elemento_organizacional i on (h.elemento_organizacional_id=i.id)
where i.id = 33 ''')

    cursos_siga = cursor.fetchall()
    campus_id = campus_cnpj[0]

    for curso_siga in cursos_siga:

        curso = Curso.objects.filter(id=int(curso_siga[0]))
        if len(curso) > 0:
            continue

        curso = Curso()

        curso.id = curso_siga[0]
        curso.nome = curso_siga[1]
        curso.finalidade = curso_siga[2]
        curso.abreviado = curso_siga[3]
        curso.tempo_maximo = curso_siga[4]
        curso.tempo_minimo = curso_siga[5]
        curso.campus_id = campus_id

        curso.save()

#Cursos de São Francisco
    cursor.execute('''select d.id as id, d.nome as nome,d.finalidade as finalidade,d.nome_reduzido as abreviado,
d.tempo_maximo as tempo_maximo, d.tempo_minimo as tempo_minimo, g.id as campus_id
from curso d
left join unidade_organizacional e on (d.unidade_organizacional_id=e.elemento_organizacional_id)
left join elemento_organizacional f on (e.elemento_organizacional_id=f.id)
left join elemento_organizacional g on (f.elemento_organizacional_id=g.id)
where g.id = 52''')

    cursos_siga = cursor.fetchall()

    campus_id = campus_cnpj[1]

    for curso_siga in cursos_siga:

        curso = Curso.objects.filter(id=int(curso_siga[0]))
        if len(curso) > 0:
            continue

        curso = Curso()

        curso.id = curso_siga[0]
        curso.nome = curso_siga[1]
        curso.finalidade = curso_siga[2]
        curso.abreviado = curso_siga[3]
        curso.tempo_maximo = curso_siga[4]
        curso.tempo_minimo = curso_siga[5]
        curso.campus_id = campus_id

        curso.save()


    #Orientador
    cursor.execute('''select d.id as id, d.nome as nome,d.email as email,(select numero from documento_identificacao
where tipo_doc_identificacao='CPF' and pessoa_fisica_id=d.id) as cpf
from docente a left join vinculo b on (a.vinculo_id = b.id)
left join funcionario c on (c.pessoa_fisica_id=b.funcionario_id)
left join pessoa_fisica d on (c.pessoa_fisica_id=d.id) where b.cargo_id=1 and d.email != '' ''')
    names = cursor.fetchall()

    for linha in names:
        cpf = corrigi_cpf(linha[3])
        orien = Orientador.objects.filter(cpf=cpf)

        if len(orien) > 0:
            continue

        orien = Orientador()
        orien.id = linha[0]
        orien.first_name = linha[1]
        orien.email = linha[2]
        orien.cpf = cpf

        orien.username = linha[2]
        orien.set_password(linha[2])

        orien.save()
        User(orien.id).groups.add(settings.PERM_GRUPO_ORIENTADOR)

    #Alunos de Araquari
    cursor.execute('''
select j.id as id,j.nome as nome, j.email as email, (select numero from documento_identificacao
where tipo_doc_identificacao='CPF' and pessoa_fisica_id=j.id limit 1) as cpf from aluno a
inner join matricula b on (a.pessoa_fisica_id = b.aluno_id and b.status_aluno_curso_id=7)
inner join matriz_curricular c on (b.matriz_curricular_id=c.id)
inner join curso d on (c.curso_id=d.id)
inner join unidade_organizacional e on (d.unidade_organizacional_id=e.elemento_organizacional_id)
inner join elemento_organizacional f on (e.elemento_organizacional_id=f.id)
inner join elemento_organizacional g on (f.elemento_organizacional_id=g.id)
inner join elemento_organizacional h on (g.elemento_organizacional_id=h.id)
inner join elemento_organizacional i on (h.elemento_organizacional_id=i.id)
inner join pessoa_fisica j on (a.pessoa_fisica_id=j.id) where j.email !='' and i.id = 33 ''')

    alunos_siga = cursor.fetchall()

    for aluno_siga in alunos_siga:

        user = User.objects.filter(username=aluno_siga[2])

        if len(user) > 0:
            continue

        cpf = corrigi_cpf(aluno_siga[3])

        aluno = Aluno.objects.filter(cpf=cpf)

        if len(aluno) > 0:
            continue

        aluno = Aluno()
        aluno.id = aluno_siga[0]
        aluno.first_name = aluno_siga[1]
        aluno.email = aluno_siga[2]
        aluno.cpf = cpf

        aluno.username = aluno_siga[2]
        aluno.set_password(aluno_siga[2])

        aluno.save()
        User(aluno.id).groups.add(settings.PERM_GRUPO_ALUNO)

    #Alunos de São Francisco
    cursor.execute('''select j.id as id,j.nome as nome, j.email as email, (select numero from documento_identificacao
where tipo_doc_identificacao='CPF' and pessoa_fisica_id=j.id limit 1) as cpf from aluno a
inner join matricula b on (a.pessoa_fisica_id = b.aluno_id and b.status_aluno_curso_id=7)
inner join matriz_curricular c on (b.matriz_curricular_id=c.id)
inner join curso d on (c.curso_id=d.id)
inner join unidade_organizacional e on (d.unidade_organizacional_id=e.elemento_organizacional_id)
inner join elemento_organizacional f on (e.elemento_organizacional_id=f.id)
inner join elemento_organizacional g on (f.elemento_organizacional_id=g.id)
inner join pessoa_fisica j on (a.pessoa_fisica_id=j.id) where j.email !='' and g.id = 52 ''')

    alunos_siga = cursor.fetchall()

    for aluno_siga in alunos_siga:

        user = User.objects.filter(username=aluno_siga[2])

        if len(user) > 0:
            continue

        cpf = corrigi_cpf(aluno_siga[3])
        aluno = Aluno.objects.filter(cpf=cpf)

        if len(aluno) > 0:
            continue

        aluno = Aluno()
        aluno.id = aluno_siga[0]
        aluno.first_name = aluno_siga[1]
        aluno.email = aluno_siga[2]
        aluno.cpf = cpf

        aluno.username = aluno_siga[2]
        aluno.set_password(aluno_siga[2])

        aluno.save()
        User(aluno.id).groups.add(settings.PERM_GRUPO_ALUNO)

    db.close()

    return redirect('/')