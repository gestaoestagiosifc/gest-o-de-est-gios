from django.db import models
from relatorio import Relatorio
from status_relatorio import StatusRelatorio
from django.contrib.auth.models import User


class LogRelatorioStatus(models.Model):
    relatorio = models.ForeignKey(Relatorio)
    status = models.ForeignKey(StatusRelatorio)
    usuario = models.ForeignKey(User)
    observacao = models.TextField(null=True)
    data = models.DateTimeField()

    class Meta:
        app_label = 'estagioapp'