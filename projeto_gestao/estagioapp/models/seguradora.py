from django.db import models


class Seguradora(models.Model):
    cnpj = models.CharField(max_length=14)
    nome = models.CharField(max_length=200)
    email = models.EmailField()

    class Meta:
        app_label = 'estagioapp'