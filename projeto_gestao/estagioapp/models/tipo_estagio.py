from django.db import models


class TipoEstagio(models.Model):
    descricao = models.CharField(max_length=100)

    class Meta:
        app_label = 'estagioapp'