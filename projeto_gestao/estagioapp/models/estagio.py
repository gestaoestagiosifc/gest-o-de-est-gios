from django.db import models
from turno import Turno
from tipo_estagio import TipoEstagio


class Estagio(models.Model):
    anexo_termo = models.FileField(upload_to='estagio/', null=True)
    data = models.DateTimeField()
    setor = models.CharField(max_length=200)
    turno = models.ForeignKey(Turno)
    tipo = models.ForeignKey(TipoEstagio)
    codigo_verificao = models.CharField(max_length=100, null=True)

    class Meta:
        app_label = 'estagioapp'