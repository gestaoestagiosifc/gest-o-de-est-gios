from django.db import models
from relatorio import Relatorio
from orientador import Orientador


class RelatorioOrientador(models.Model):
    relatorio = models.ForeignKey(Relatorio)
    orientador = models.ForeignKey(Orientador)
    descricao = models.TextField(null=True)
    data = models.DateTimeField()

    class Meta:
        app_label = 'estagioapp'