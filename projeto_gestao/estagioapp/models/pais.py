from django.db import models


class Pais(models.Model):
    nome = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nome

    class Meta:
        app_label = 'estagioapp'