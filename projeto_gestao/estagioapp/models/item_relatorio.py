from django.db import models
from relatorio import Relatorio


class ItemRelatorio(models.Model):
    relatorio = models.ForeignKey(Relatorio)
    atividade = models.TextField(null=True)
    dificuldades = models.TextField(null=True)
    contribuicao = models.TextField(null=True)
    conhecimento_necessario = models.TextField(null=True)

    abordado_curso = models.BooleanField()

    introducao = models.TextField(null=True)
    justificativa = models.TextField(null=True)
    objetivo = models.TextField(null=True)
    comentario_geral = models.TextField(null=True)
    data = models.DateTimeField()

    class Meta:
        app_label = 'estagioapp'