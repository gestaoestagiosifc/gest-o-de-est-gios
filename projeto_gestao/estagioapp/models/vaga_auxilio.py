from django.db import models
from vaga import Vaga
from auxilio import Auxilio


class VagaAuxilio(models.Model):
    vaga = models.ForeignKey(Vaga)
    auxilio = models.ForeignKey(Auxilio)
    valor = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        app_label = 'estagioapp'