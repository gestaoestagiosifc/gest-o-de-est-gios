from django.db import models
from django.conf import settings
from datetime import datetime, timedelta
from pessoa import Pessoa
from tipo_concendente import TipoConcedente
from endereco import Endereco
from contato import Contato


class Concedente(Pessoa):
    cnpj = models.CharField(max_length=18, null=True)
    crea = models.CharField(max_length=50, null=True)
    nome_empresa = models.CharField(max_length=200, null=True)
    tipo_concedente = models.ForeignKey(TipoConcedente)
    endereco = models.ForeignKey(Endereco)
    area_atuacao = models.CharField(max_length=200)
    contato = models.ManyToManyField(Contato)

    def get_convenio_atual(self, id_concedente=None):
        if id_concedente is None:
            id_concedente = self.id

    	from log_convenio_status import LogConvenioStatus
    	log = LogConvenioStatus.objects.filter(convenio__concedente_id=id_concedente).order_by('-data').first()
    	return log

    def permitir_adicionar_oferta(self, id_concedente=None):
        years = 5
        dias_por_ano = 365
        if id_concedente is None:
            id_concedente = self.id

        log = self.get_convenio_atual(id_concedente)

        if log.status_id == settings.ST_CONVENIO_APROVADO:
            data_vencimento = log.data + timedelta(days=years*dias_por_ano)
            if data_vencimento > datetime.today():
                return True 
        return False

    class Meta:
        app_label = 'estagioapp'



