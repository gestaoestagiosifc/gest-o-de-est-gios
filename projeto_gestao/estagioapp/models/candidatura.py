from django.db import models
from django.core.paginator import Paginator
from django.conf import settings
from django.db.models import Q
from vaga import Vaga
from aluno import Aluno


class Candidatura(models.Model):
    aluno = models.ForeignKey(Aluno)
    vaga = models.ForeignKey(Vaga)
    data = models.DateTimeField(auto_now=True)

    ''' Este metodo so funciona quando executar a consulta de candidatos por vaga '''
    def contratado(self):

        from log_vaga_status import LogVagaStatus
        candidatura = LogVagaStatus.objects.raw(''' select * from estagioapp_logvagastatus where vaga_id=%s
            order by data desc limit 1 ''', [self.vaga_id])

        candidatura = list(candidatura)

        if len(candidatura) > 0 and int(candidatura[0].status_id) == settings.ST_VAGA_PREECHIDA:
            return True

        return False

    def get_candidaturas_nao_preenchidas(self, concedente_id):
        candidatura = Candidatura.objects.raw(''' 
            select c.*, f.descricao from estagioapp_candidatura c 
            inner join estagioapp_vaga v on v.id = c.vaga_id 
            inner join estagioapp_funcao f on f.id = v.funcao_id
            inner join estagioapp_logvagastatus lvs on lvs.vaga_id = v.id 
            inner join estagioapp_statusvaga sv on sv.id = lvs.status_id
            inner join estagioapp_oferta o on v.oferta_id = o.id
            where lvs.status_id = %s and o.concedente_id = %s
        ''', [settings.ST_VAGA_NAO_PREENCHIDA ,int(concedente_id)])

        return list(candidatura)

    def get_candidatos_por_vaga(self, id_vaga):
        candidatos = Candidatura.objects.raw(''' 
            select a.*, (select status_id from estagioapp_logvagastatus where vaga_id=a.vaga_id
            order by data desc limit 1) as status_id from estagioapp_candidatura a  where a.vaga_id = %s
        ''', [id_vaga])

        return list(candidatos)

    def consulta_vagas_do_aluno(self, id_aluno, page, procurar, classificacao):
        sql =  'select v.id, v.descricao as "desc_vaga",'
        sql += 'f.descricao as "desc_funcao",'
        sql += 'e.nome_empresa, sv.descricao as "desc_status" '
        sql += 'from estagioapp_candidatura c '
        sql += 'inner join estagioapp_vaga v on v.id = c.vaga_id '
        sql += 'inner join estagioapp_funcao f on f.id = v.funcao_id '
        sql += 'inner join estagioapp_oferta o on v.oferta_id = o.id '
        sql += 'inner join estagioapp_concedente e on o.concedente_id = e.pessoa_ptr_id '
        sql += 'inner join estagioapp_logvagastatus lvs on lvs.vaga_id = v.id '
        sql += 'inner join estagioapp_statusvaga sv on sv.id = lvs.status_id '
        sql += 'where lvs.id = (select id from estagioapp_logvagastatus where vaga_id = v.id order by data desc limit 1) '
        sql += 'and c.aluno_id = %s '

        order_by = ''
        if classificacao is not None and classificacao != '':
            if classificacao == '0':
                order_by += 'order by desc_vaga asc'
            elif classificacao == '1':
                order_by += 'order by desc_vaga desc'

        if procurar is not None and procurar != '':
            procurar = '%'+procurar+'%'
            sql += 'and UPPER(v.descricao) like UPPER(%s) '
            sql += order_by
            vagas = Candidatura.objects.raw(sql, [int(id_aluno), procurar])
        else:
            sql += order_by
            vagas = Candidatura.objects.raw(sql, [int(id_aluno)])

        paginator = Paginator(list(vagas), settings.NR_REGISTROS_PAGINA)

        try:
            vagas_page = paginator.page(page)
        except:
            vagas_page = paginator.page(paginator.num_pages)

        return vagas_page


    class Meta:
        app_label = 'estagioapp'