from django.db import models
from status_estagio import StatusEstagio
from estagio import Estagio
from django.contrib.auth.models import User


class LogEstagioStatus(models.Model):
    estagio = models.ForeignKey(Estagio)
    data = models.DateTimeField()
    status = models.ForeignKey(StatusEstagio)
    observacao = models.TextField(null=True)
    usuario = models.ForeignKey(User)

    class Meta:
        app_label = 'estagioapp'