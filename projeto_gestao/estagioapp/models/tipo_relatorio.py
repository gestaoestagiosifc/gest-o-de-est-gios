from django.db import models


class TipoRelatorio(models.Model):
    descricao = models.CharField(max_length=100)

    class Meta:
        app_label = 'estagioapp'