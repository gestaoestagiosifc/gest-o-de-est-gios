from django.db import models
from django.core.paginator import Paginator
from concedente import Concedente
from django.db.models import Q
from django.conf import settings


class Convenio(models.Model):
    anexo = models.FileField(upload_to='convenio/', null=True)
    observacao = models.TextField()
    concedente = models.ForeignKey(Concedente)

    def get_status_atual(self, id=None):
        from log_convenio_status import LogConvenioStatus

        if id is None:
            id = self.id

        log = LogConvenioStatus.objects.filter(convenio_id=id).order_by('-data').first()

        return log

    def get_data_solicitacao(self, id=None):
        from log_convenio_status import LogConvenioStatus

        if id is None:
            id = self.id

        log = LogConvenioStatus.objects.filter(convenio_id=id).order_by('data').values('data').first()

        if log is not None:
            return log['data']

        return log

    def get_page(self, page, procurar, classificacao):

        sql = 'SELECT a.id as id, a.anexo as anexo, a.observacao as observacao, a.concedente_id as concedente_id, '
        sql += '(select data from estagioapp_logconveniostatus where convenio_id=a.id order by data asc limit 1) as data_solicitacao, '
        sql += '(select data from estagioapp_logconveniostatus where convenio_id=a.id order by data desc limit 1) as data_modificacao, '
        sql += '(select st.classe_css from estagioapp_logconveniostatus as log '
        sql += 'inner join estagioapp_statusconvenio as st on (log.status_id=st.id) '
        sql += 'where convenio_id=a.id order by data desc limit 1) as status_classe_css '
        sql += 'FROM estagioapp_convenio as a '
        sql += 'INNER JOIN estagioapp_concedente as b ON ( a.concedente_id = b.pessoa_ptr_id ) '
        sql += 'INNER JOIN estagioapp_pessoa as c ON ( b.pessoa_ptr_id = c.user_ptr_id ) '
        sql += 'INNER JOIN auth_user d ON ( c.user_ptr_id = d.id ) '

        order_by = ''

        if classificacao is not None and classificacao != '':
            order_by += 'order by '
            if classificacao == '0':
                order_by += 'b.nome_empresa asc'
            elif classificacao == '1':
                order_by += 'b.nome_empresa desc'
            elif classificacao == '2':
                order_by += 'data_solicitacao'
            elif classificacao == '3':
                order_by += 'data_modificacao'

        if procurar is not None:

            procurar = '%'+procurar+'%'
            sql += 'WHERE UPPER(d.first_name) LIKE UPPER(%s) OR '
            sql += 'UPPER(b.nome_empresa) LIKE UPPER(%s) OR '
            sql += 'UPPER(b.cnpj) LIKE UPPER(%s) OR '
            sql += 'UPPER(c.cpf) LIKE UPPER(%s) '
            sql += order_by

            convenios = Convenio.objects.raw(sql, [procurar, procurar, procurar, procurar])

        else:
            sql += order_by
            convenios = Convenio.objects.raw(sql)

        paginator = Paginator(list(convenios), settings.NR_REGISTROS_PAGINA)

        try:
            convenios_page = paginator.page(page)
        except:
            convenios_page = paginator.page(paginator.num_pages)

        return convenios_page

    class Meta:
        app_label = 'estagioapp'

    def get_convenio(self, id):
        return Convenio.objects.get(pk=id)
