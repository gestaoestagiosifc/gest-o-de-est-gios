from django.db import models
from relatorio import Relatorio
from supervisor import Supervisor
from django.contrib.auth.models import User


class RelatorioSupervisor(models.Model):
    relatorio = models.ForeignKey(Relatorio)
    supervisor = models.ForeignKey(Supervisor)
    data = models.DateTimeField()
    descricao = models.TextField(null=True)


    class Meta:
        app_label = 'estagioapp'