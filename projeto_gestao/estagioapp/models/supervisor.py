from django.db import models
from pessoa import Pessoa
from concedente import Concedente


class Supervisor(Pessoa):
    concedente = models.ForeignKey(Concedente)
    cargo = models.CharField(max_length=200)

    class Meta:
        app_label = 'estagioapp'