from django.db import models
from convenio import Convenio
from status_convenio import StatusConvenio
from django.contrib.auth.models import User


class LogConvenioStatus(models.Model):
    convenio = models.ForeignKey(Convenio)
    status = models.ForeignKey(StatusConvenio)
    data = models.DateTimeField(auto_now=True)
    observacao = models.TextField(null=True)
    usuario = models.ForeignKey(User, null=True)

    class Meta:
        app_label = 'estagioapp'