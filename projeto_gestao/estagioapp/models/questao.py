from django.db import models


class Questao(models.Model):
    descricao = models.CharField(max_length=250)

    class Meta:
        app_label = 'estagioapp'