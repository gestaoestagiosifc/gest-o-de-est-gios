from django.db import models


class TipoContato(models.Model):
    descricao = models.CharField(max_length=100)

    def __unicode__(self):
        return self.descricao

    class Meta:
        app_label = 'estagioapp'