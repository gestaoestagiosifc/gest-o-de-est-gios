from django.db import models
from endereco import Endereco
from seguradora import Seguradora
from contato import Contato


class Campus(models.Model):
    cnpj = models.CharField(max_length=14, primary_key=True)
    nome = models.CharField(max_length=200)
    endereco = models.ForeignKey(Endereco)
    seguradora = models.ForeignKey(Seguradora)
    contatos = models.ManyToManyField(Contato)

    class Meta:
        app_label = 'estagioapp'