from django.db import models
from status_curso import StatusCurso
from campus import Campus


class Curso(models.Model):
    nome = models.CharField(max_length=200)
    finalidade = models.TextField()
    tempo_maximo = models.IntegerField()
    tempo_minimo = models.IntegerField()
    abreviado = models.CharField(max_length=50)
    status = models.ForeignKey(StatusCurso, null=True)
    campus = models.ForeignKey(Campus)

    class Meta:
            app_label = 'estagioapp'