from django.db import models
from concedente import Concedente
from django.db.models import Q
from django.conf import settings
from django.core.paginator import Paginator


class Oferta(models.Model):
    titulo = models.CharField(max_length=200)
    concedente = models.ForeignKey(Concedente)
    data = models.DateTimeField()
    descricao = models.TextField()
    data_vencimento = models.DateField()
    liberado = models.BooleanField()

    def get_total_vagas(self):
        from vaga import Vaga
        total_vagas = Vaga.objects.filter(oferta_id=self.id).count()
        return total_vagas

    def get_vagas(self):
        from vaga import Vaga

        quantidade_vagas = Vaga.objects.raw('''
            SELECT v.id as id FROM estagioapp_vaga AS v
            WHERE v.oferta_id = %s AND (SELECT status_id FROM estagioapp_logvagastatus
            WHERE vaga_id = v.id ORDER BY data DESC limit 1) = %s
            AND v.id NOT IN (SELECT cand.vaga_id FROM estagioapp_candidatura AS cand
            INNER JOIN estagioapp_vaga v ON cand.vaga_id = v.id
            WHERE v.oferta_id = %s)
        ''', [int(self.id), settings.ST_VAGA_NAO_PREENCHIDA, int(self.id)])

        return list(quantidade_vagas)

    def get_vagas_editar(self):
        from vaga import Vaga

        vagas = Vaga.objects.raw(''' 
            SELECT v.* as id FROM estagioapp_vaga AS v
            WHERE v.oferta_id = %s AND (SELECT status_id FROM estagioapp_logvagastatus
            WHERE vaga_id = v.id ORDER BY data DESC limit 1) = %s
        ''', [self.id, settings.ST_VAGA_NAO_PREENCHIDA])

        return list(vagas)

    def get_total_vagas_disponiveis(self):
        return len(self.get_vagas())


    def get_total_vagas_preenchidas(self):
        from vaga import Vaga

        quantidade_vagas = Vaga.objects.raw('''
            SELECT v.id as id FROM estagioapp_vaga AS v
            WHERE v.oferta_id = %s AND (SELECT status_id FROM estagioapp_logvagastatus
            WHERE vaga_id = v.id ORDER BY data DESC limit 1) != %s
            
        ''', [int(self.id), settings.ST_VAGA_PREECHIDA])

        return len(list(quantidade_vagas))

    def consulta_aluno_page(self, page, procurar, classificacao):

        if procurar is not None:

            ofertas = Oferta.objects.filter(Q(titulo__icontains=procurar) |
                                            Q(descricao__icontains=procurar) |
                                            Q(concedente__first_name__icontains=procurar) |
                                            Q(concedente__nome_empresa__icontains=procurar),
                                            liberado=True)

        else:
            ofertas = Oferta.objects.filter(liberado=True)

        if classificacao is not None and classificacao != '':
            if classificacao == '0':
                ofertas = ofertas.order_by('-titulo')
            elif classificacao == '1':
                ofertas = ofertas.order_by('titulo')
            elif classificacao == '2':
                ofertas = ofertas.order_by('data')

        paginator = Paginator(ofertas, settings.NR_REGISTROS_PAGINA)

        try:
            ofertas_page = paginator.page(page)
        except:
            ofertas_page = paginator.page(paginator.num_pages)

        return ofertas_page

    class Meta:
        app_label = 'estagioapp'