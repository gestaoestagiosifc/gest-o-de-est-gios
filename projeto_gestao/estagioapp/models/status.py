from django.db import models


class Status(models.Model):
    descricao = models.CharField(max_length=200)
    classe_css = models.CharField(max_length=30, null=True)

    class Meta:
        abstract = True
        app_label = 'estagioapp'