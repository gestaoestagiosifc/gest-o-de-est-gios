from django.db import models
from campus import Campus
from coordenador import Coordenador


class CampusCoordenador(models.Model):
    campus = models.ForeignKey(Campus)
    coordenador = models.ForeignKey(Coordenador)
    dt_entrada = models.DateTimeField()
    dt_saida = models.DateTimeField(null=True)

    class Meta:
        app_label = 'estagioapp'