from django.db import models
from estagio import Estagio
from tipo_relatorio import TipoRelatorio


class Relatorio(models.Model):
    data = models.DateTimeField()
    departamento_setor = models.CharField(max_length=200)
    estagio = models.ForeignKey(Estagio)
    tipo = models.ForeignKey(TipoRelatorio)

    class Meta:
        app_label = 'estagioapp'