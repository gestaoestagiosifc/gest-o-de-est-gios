from django.db import models
from tipo_contato import TipoContato


class Contato(models.Model):
    contato = models.CharField(max_length=200)
    tipo_contato = models.ForeignKey(TipoContato)


    class Meta:
        app_label = 'estagioapp'


    def __unicode__(self):
        return self.contato