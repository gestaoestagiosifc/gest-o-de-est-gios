from django.db import models
from estagio import Estagio
from orientador import Orientador


class EstagioOrientador(models.Model):
    estagio = models.ForeignKey(Estagio)
    orientador = models.ForeignKey(Orientador)
    data = models.DateField(null=True)
    observacao = models.TextField(null=True)

    class Meta:
        app_label = 'estagioapp'