from django.db import models
from vaga import Vaga
from status_vaga import StatusVaga


class LogVagaStatus(models.Model):
    vaga = models.ForeignKey(Vaga)
    status = models.ForeignKey(StatusVaga)
    data = models.DateTimeField(auto_now=True)
    observacao = models.TextField(null=True)

    class Meta:
        app_label = 'estagioapp'