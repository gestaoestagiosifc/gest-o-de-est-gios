from django.db import models
from turno import Turno
from oferta import Oferta
from funcao import Funcao
from auxilio import Auxilio



class Vaga(models.Model):
    descricao = models.TextField()
    hora_inicio = models.TimeField()
    hora_fim = models.TimeField()
    oferta = models.ForeignKey(Oferta)
    turno = models.ForeignKey(Turno)
    funcao = models.ForeignKey(Funcao)
    auxilios = models.ManyToManyField(Auxilio, through="VagaAuxilio")

    def get_auxilios(self):
        from vaga_auxilio import VagaAuxilio
        auxilios = VagaAuxilio.objects.filter(vaga_id=self.id)
        return auxilios

    class Meta:
        app_label = 'estagioapp'