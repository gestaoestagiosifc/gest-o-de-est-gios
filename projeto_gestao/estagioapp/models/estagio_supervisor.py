from django.db import models
from supervisor import Supervisor
from estagio import Estagio


class EstagioSupervisor(models.Model):
    supervisor = models.ForeignKey(Supervisor)
    estagio = models.ForeignKey(Estagio)
    observacao = models.TextField(null=True)
    data = models.DateTimeField(null=True)

    class Meta:
        app_label = 'estagioapp'