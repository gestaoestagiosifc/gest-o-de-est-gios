from django.db import models
from relatorio import Relatorio
from questao import Questao


class RelatorioQuestao(models.Model):
    relatorio = models.ForeignKey(Relatorio)
    questao = models.ForeignKey(Questao)
    resposta = models.BooleanField()
    justificativa = models.CharField(max_length=250)

    class Meta:
        app_label = 'estagioapp'