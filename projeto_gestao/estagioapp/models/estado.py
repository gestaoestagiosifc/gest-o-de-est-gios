from django.db import models
from pais import Pais


class Estado(models.Model):
    abreviado = models.CharField(max_length=10)
    nome = models.CharField(max_length=200)
    pais = models.ForeignKey(Pais)

    def __unicode__(self):
        return self.abreviado

    class Meta:
        app_label = 'estagioapp'