from django.db import models
from estagio import Estagio
from auxilio import Auxilio


class EstagioAuxilio(models.Model):
    estagio = models.ForeignKey(Estagio)
    auxilio = models.ForeignKey(Auxilio)
    valor = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        app_label = 'estagioapp'