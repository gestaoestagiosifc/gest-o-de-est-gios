from django.db import models
from django.contrib.auth.models import User


class Pessoa(User):
    cpf = models.CharField(max_length=14, unique=True)

    class Meta:
        app_label = 'estagioapp'