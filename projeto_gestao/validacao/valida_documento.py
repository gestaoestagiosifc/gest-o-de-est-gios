from cnpj import CNPJ
from cpf import CPF


def valida(doc):

    doc_valido = CPF(doc).isValid()

    if doc_valido is False:
        try:
            doc_valido = CNPJ(doc).valido()
        except:
            doc_valido = False

    return doc_valido