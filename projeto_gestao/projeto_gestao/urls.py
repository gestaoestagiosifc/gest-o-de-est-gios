from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

from estagioapp.views import AvaliacaoConvenioView
from estagioapp.views import ConsultaConveniosView
from estagioapp.views import SolicitacaoConvenioView
from estagioapp.views import MeuConvenioView
from estagioapp.views import ImprimirConvenioView
from estagioapp.views import DownloadConvenioView


from estagioapp.views import CadastrarOfertaView
from estagioapp.views import ConsultarOfertasEmpresaView
from estagioapp.views import ListarOfertaView
from estagioapp.views import EditarOfertaView
from estagioapp.views import DetalheOfertaView

from estagioapp.views import DetalheVagaView
from estagioapp.views import MinhasVagasView
from estagioapp.views import CandidatarVagaView
from estagioapp.views import CadastrarVagaView
from estagioapp.views import EditarVagaView
from estagioapp.views import ExcluirVagaView

from estagioapp.views import InserirAuxilioView
from estagioapp.views import ListarAuxiliosView
from estagioapp.views import EditarAuxilioView
from estagioapp.views import ExcluirAuxilioView

from estagioapp.views import CandidatosView
from estagioapp.views import DetalheCandidatoView
from estagioapp.views import ContratarCandidatoView
from estagioapp.views import CandidatosPorVagaView

from estagioapp.views import ConsultaConcedenteView

from estagioapp.views import ImprimirTermoView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'projeto_gestao.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^$', 'estagioapp.views.estagio_app.index', name='index'),
    url(r'^home/$', 'estagioapp.views.estagio_app.home', name='home'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login/login.html'}, name='login'),
    url(r'^logout/$', 'estagioapp.views.estagio_app.logout_user', name='logout_user'),

    url(r'^solicitacao-convenio/$', SolicitacaoConvenioView.as_view(), name='solicitacao-convenio'),
    url(r'^avaliacao-convenio/(?P<id>\d+)/$', AvaliacaoConvenioView.as_view(), name='avaliacaoconvenio'),
    url(r'^meu-convenio/$', MeuConvenioView.as_view(), name='meuconvenio'),
    url(r'^consulta-convenios/$', ConsultaConveniosView.as_view(), name='consulta-convenios'),
    url(r'^imprimir-convenio/(?P<id>\d+)/$', ImprimirConvenioView.as_view(), name='imprimir-convenio'),

    url(r'^download-convenio/(?P<id>\d+)/$', DownloadConvenioView.as_view(), name='download-convenio'),


    url(r'^cadastra-oferta/$', CadastrarOfertaView.as_view(), name='cadastraoferta'),
    url(r'^editar-oferta/(?P<id_oferta>\d+)/$', EditarOfertaView.as_view(), name='editaroferta'),
    url(r'^listar-ofertas/$', ListarOfertaView.as_view(), name='listarofertas'),
    url(r'^consultar-ofertas/$', ConsultarOfertasEmpresaView.as_view(), name='consultarofertas'),
    url(r'^detalhe-oferta/(?P<id_oferta>\d+)/$', DetalheOfertaView.as_view(), name='detalheoferta'),

    url(r'^minhas-vagas/$', MinhasVagasView.as_view(), name='minhasvagas'),
    url(r'^detalhe-vaga/(?P<id_vaga>\d+)/$', DetalheVagaView.as_view(), name='detalhevaga'),
    url(r'^candidatar-vaga/(?P<id_vaga>\d+)/$', CandidatarVagaView.as_view(), name='candidatarvaga'),
    url(r'^cadastrar-vaga/(?P<id_oferta>\d+)/$', CadastrarVagaView.as_view(), name='cadastrarvaga'),
    url(r'^editar-vaga/(?P<id_vaga>\d+)/$', EditarVagaView.as_view(), name='editarvaga'),
    url(r'^excluir-vaga/(?P<id_vaga>\d+)/$', ExcluirVagaView.as_view(), name='excluirvaga'),

    url(r'^inserir_auxilio/(?P<id_vaga>\d+)/$', InserirAuxilioView.as_view(), name='inserirauxilio'),
    url(r'^inserir-auxilio-ao-editar/(?P<id_vaga>\d+)/$', InserirAuxilioView.as_view(), name='novoauxilio'),
    url(r'^auxilios/(?P<id_vaga>\d+)/$', ListarAuxiliosView.as_view(), name='auxilios'),
    url(r'^editar-auxilio/(?P<pk>\d+)/$', EditarAuxilioView.as_view(), name='editarauxilio'),
    url(r'^excluir-auxilio/(?P<pk>\d+)/$', ExcluirAuxilioView.as_view(), name='excluirauxilio'),

    url(r'^candidatos/$', CandidatosView.as_view(), name='candidatos'),
    url(r'^detalhe-candidato/(?P<id_candidatura>\d+)/$', DetalheCandidatoView.as_view(), name='detalhecandidato'),
    url(r'^contratar/(?P<id_vaga>\d+)/$', ContratarCandidatoView.as_view(), name='contratar'),
    url(r'^candidatos-por-vaga/(?P<id_vaga>\d+)/$', CandidatosPorVagaView.as_view(), name='candidatosporvaga'),

    url(r'^empresas/$', ConsultaConcedenteView.as_view(), name='empresas'),

    url(r'^carrega-cidades/$', 'estagioapp.views.carrega_estado_cidade.carrega_estado_cidade', name='carrega-cidades'),

    url(r'^cargainicial/$', 'estagioapp.views.carga_inicial.carga', name='carga'),
    url(r'^importasiga/$', 'estagioapp.views.carga_inicial.importa_dados_sigaedu', name='importa_dados_sigaedu'),

    url(r'^imprimir-termo/(?P<id>\d+)/$', ImprimirTermoView.as_view(), name='imprimir-termo'),
    # url(r'^/'),
)
