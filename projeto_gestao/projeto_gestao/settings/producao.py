from .base import *

# Constantes de permicoes de grupos

PERM_GRUPO_ALUNO = 1
PERM_GRUPO_ORIENTADOR = 2
PERM_GRUPO_COORDENADOR = 3
PERM_GRUPO_REITORIA = 4
PERM_GRUPO_EMPRESA = 5
PERM_GRUPO_SUPERVISOR = 6


# Constantes de configiracao de status

ST_CONVENIO_NAO_AVALIADO = 1
ST_CONVENIO_EM_ANDAMENTO = 2
ST_CONVENIO_APROVADO = 3
ST_CONVENIO_RECUSADO = 4
ST_CONVENIO_CANCELADO = 5

ST_VAGA_NAO_PREENCHIDA = 1
ST_VAGA_PREECHIDA = 2
ST_VAGA_CANCELADA = 3

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'gestaoestagio6',                      # Or path to database file if using sqlite3.
        'USER': 'postgres',                      # Not used with sqlite3.
        'PASSWORD': 'root',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}
