$(document).ready(function(){

    var path = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');

    $('select[name=estado]').on('change', function() {

        $.ajax({
            url: path + "/carrega-cidades/", // url
            type: 'POST',
            dataType: 'json',
            data: {estado: $(this).val()},
            beforeSend: function () {
                $("select[name=cidade]").html("<option value=''>Carregando...</option>");
            },
            success: function (data, textStatus, xhr) {
                var html = null;
                var cidade = data;

                $(cidade).each(function (key, val) {
                    html += '<option value="' + val.pk + '">' + val['fields']['nome'] + '</option>';
                });
                $('select[name=cidade]').html(html);

            },
            error: function (xhr, textStatus, errorThrown) {
                $('select[name=cidade]').html('<option value="">Nenhuma cidade cadastrada para este estado</option>');
            }
        });
    });

    $('#cep').mask('00000-000');
    $('#cpf').mask('000.000.000-00');
    $('#cnpj').mask('00.000.000/0000-00');
});

function config_contato(contato_name){

    tipo_contato = $('select[name=tipo_'+contato_name+']').val();

    placeholder = '';
    mask = '';
    type = '';
    switch (parseInt(tipo_contato)){

        case 1://E-mail
            placeholder = 'email@dominio.com';
            type = 'email';
            break;
        case 2://Telefone
            placeholder = '(00) 0000-0000';
            type = 'phone';
            mask = '(00) 0000-0000';
            break;
        case 3://Celular
            placeholder = '(00) 0000-0000';
            type = 'phone';
            mask = '(00) 0000-0000';
            break;
    }
    $('input[name='+contato_name+']').attr("placeholder",placeholder);
    //$('input[name='+contato_name+']').attr("type",type);

    if(mask !=''){
        $('input[name='+contato_name+']').mask(mask);
    }
    else{
         $('input[name='+contato_name+']').unmask(mask);

    }
}


