$(document)

    .ready(function() {

       $('.ui.radio.checkbox')
          .checkbox()
        ;


        $('.errorlist').addClass('ui red pointing above ui label')

        var numero_crea, uf_crea;

        // id=$('article').attr('id')
        // $('nav #'+id).addClass('active')

        $('.ui.dropdown')
          .dropdown()
        ;

        $('.ui.accordion')
          .accordion()
        ;

        

        //Habilita a navegação nos steps

        $('.ui.labeled.icon.button')
          .tab()

          $('.filter.menu .step')
          .tab()
          ;

        ;

       

        // quando template validar estiver carregado, verificar se possui ou não valores pré estabelecidos

        $('.validar').ready(function(){


            if ($('#valido').is(':checked')) {

                $('#textarea').hide();
                $('textarea[name=obs]').removeAttr('required');
                $('#acoes_contrato_valido').show();

            }else{

                $('#textarea').show();
                $('#acoes_contrato_valido').hide();

            };

        });

        // Habilita campo de cnpj ou crea de acordo com a escolha do select
        var tipo_concedente = $('#tipo_concedente').val();

          if (tipo_concedente){
            if (tipo_concedente == 1 || tipo_concedente == 2 ) {
                $('.cnpj').show();
                $('.crea').hide();
            }else{  
                $('.crea').show();
                $('.cnpj').hide();
            }
          }
        

        $('#tipo_concedente').change(function(){
          
          tipo_concedente = $('#tipo_concedente').val();
          tpc = $('#tipo_concedente option:selected').text();

          if (tpc!='---------'){
            if (tipo_concedente == 1 || tipo_concedente == 2 ) {

              $('.cnpj').show();
              $('.crea').hide();

            }else{

              $('.crea').show();
              $('.cnpj').hide();

            };
          }
          else{
            $('.crea').hide();
            $('.cnpj').hide();
          }

        });


        // Pega o número do crea quando o foco sai do input 

        $("#numero_crea").blur(function() {

          uf_crea = $('#uf_crea option:selected').text();

          numero_crea = $("#numero_crea").val();
          
          var crea = numero_crea + "/" + uf_crea;

          $('#crea').val(crea);

        });


        // Pega a uf do crea com o campo select

        $('#uf_crea').change(function(){

          uf_crea = $('#uf_crea option:selected').text();

          numero_crea = $("#numero_crea").val();

          var crea = numero_crea + "/" + uf_crea;

          $('#crea').val(crea);

        });


        // separando o crea para exibição   
          var crea = $('#crea').val();
          if (crea){
            var crea_valor = crea.split('/');

            // numero do crea
            $('#numero_crea').val(crea_valor[0]);

            // uf do crea
            $('#uf_crea').val(crea_valor[1]);
          } 

       
        // se a opção 'convênio valido' for selecionada, ocultar text área para observações


        $('#textarea').show();
        $('#acoes_contrato_valido').hide();

        $('.radio').click(function(){


            if ($('#valido').is(':checked')) {

                $('#textarea').hide();
                $('textarea[name=obs]').removeAttr('required');
                $('#acoes_contrato_valido').show();

            }else{

                $('#textarea').show();
                $('#acoes_contrato_valido').hide();

            };

        });
        

        // *************** 

        $('.tabular.menu a').click(function(){
        
          $('.tabular.menu a').removeClass('active')
          $(this).addClass('active')
          id = $(this).attr('id')
          $('.conteudo').removeClass('ativa')
          $('.'+id).addClass('ativa')

        });    

        

    });

;


function confirmar ( text , url ) {
  event.preventDefault();
  alertify.confirm( text, function (e) {
    if (e) location.href = url;
  });
}