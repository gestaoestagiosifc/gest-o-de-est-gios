#coding: utf-8
from django.conf import settings
from django.template.loader import render_to_string


def get_menu(request):
    html = ''
    if request.user.is_authenticated():

        grupos_id = request.user.groups.values_list('id', flat=True).all()
        if settings.PERM_GRUPO_EMPRESA in grupos_id:
            html += render_to_string('partials/menu_concedente.html')

        if settings.PERM_GRUPO_REITORIA in grupos_id:
            html += render_to_string('partials/menu_reitoria.html')

        if settings.PERM_GRUPO_ALUNO in grupos_id:
            html += render_to_string('partials/menu_aluno.html')

        if settings.PERM_GRUPO_COORDENADOR in grupos_id:
            html += render_to_string('partials/menu_coordenador.html')

    return {'context_menu':html}